var affichage = document.getElementById('last_operation_history');
var nmChar;
var currentChar , previousChar;
var operations = ['+' , '-' , '*' , '/'];
function button_number(c){
    affichage.value += c;
    nmChar = affichage.value.length;
    currentChar = c;
    getPrevious();
}
function button_clear(){
    affichage.value = "";
}
function calc(){
    affichage.value = eval(affichage.value);
}
function getPrevious(){
    previousChar = affichage.value.substring(nmChar-2 , nmChar-1);
    checksyntax();
}
function checksyntax(){
    if ( operations.includes(currentChar) && nmChar == 1 ){
        removeChar();
    }
    if ( operations.includes(previousChar) && operations.includes(currentChar)){
        if ( previousChar == currentChar ){
            removeChar();
        }else{
            overWrite();
        }
    }

}
function overWrite(){
    affichage.value = affichage.value.slice(0 , nmChar-2) + affichage.value.slice( nmChar-1 );
}
function removeChar(){
    affichage.value = affichage.value.substring(0 , nmChar-1);
}